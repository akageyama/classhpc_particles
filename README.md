# ClassHPC_particles

神戸大学工学部情報知能工学科の講義「HPC」のためのサンプルプログラム

揺らしたヒモの運動をバネ=質点系として解く

重力下で垂らしたヒモの両端を激しく振動させと「加熱」されたヒモは縮むだろうか？

このサンプルプログラムは振動の振幅が大きすぎ、周波数も高すぎる。また、散逸（空気抵抗）の効果が入っていない。

## シミュレーションモデル
- ヒモをバネ質点系で模擬する。
- 運動方程式を4次ルンゲ=クッタ法で時間積分する。
- 時間の刻み幅はバネの固有振動数よりも十分小さくする。


